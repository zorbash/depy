depy
====

### Clone the repo

`git clone git@github.com:Zorbash/depy.git`

### Run the install script

`./setup.sh`

### Run depy

`./depy -d <path/to/be/visualized>`
