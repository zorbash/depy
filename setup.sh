#!/bin/bash

install_cmd="sudo apt-get install"
update_cmd="sudo apt-get update"

#install node
node_exists=$(which node)
if [ "$node_exists" == "" ]; then
  $update_cmd
  $install_cmd python-software-properties python g++ make
  sudo add-apt-repository ppa:chris-lea/node.js
  $update_cmd
  $sagi nodejs
fi

snig="sudo npm install -g"
$snig coffee-script

#install node dependencies
sudo npm install
sudo cp ./fix_erb /bin/fix_erb
